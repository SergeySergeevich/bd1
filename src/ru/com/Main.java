package ru.com;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class Main {
/*
a.	��� ������� ��������� ������������ ��� ������ ODBC � ���� ������, ��������� ��� ������������ � ������ �� �.2;
b.	��������� ������������� ���� ����������� ������, � ������ �� ���������� � �������;
c.	����������� ���������������� ��������� (gui/cli, �� �����), � ������������ � ��������� �������

�����: ������� � ����������� ����, �������� �������� ����� ��������� �� �. 3 (� ������������� �������) � ���� ���� ������ � ������� plain.

����������. �������� ������: �������� ������, ����(�����), ������ �������, �������, ������� ��������������.
����������� ������� ����������������� ����������: ��������������/�������� ������,
����� �� �������� �� �� ��������������, ����� �� �����, �������� � ������.
* */
    public static void main(String[] args){
        // �������� ��������� �� �����
        Properties props = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("database.properties")))
        {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");

        // �������� ������� - ����� ��� ������ � ��
        try {
            DBAdapter.setProperties(url,username,password);
            DBAdapter myDBAdapter = DBAdapter.getInstance();
            // ����� ���������
            //myDBAdapter.addRecord(new Film("Good,Bad,Ugly",new String [] {"comedy", "western"}, 8.9f,true, new String [] {"Clint Eastwood"}));
            //myDBAdapter.addRecord(new Film("Madagaskar",new String [] {"comedy", "multfilm"}, 7.9f,false, new String [] {"Alex","Glorya","Melman","Marti"}));
            //myDBAdapter.addRecord(new Film("Madagaskar 3: New War",new String [] {"comedy","military", "multfilm"}, 4.5f ,false, new String [] {"Alex","Glorya","Melman","Marti"}));
            /*myDBAdapter.addRecords(
                    new Film[]{
                            new Film("Good,Bad,Ugly",new String [] {"comedy", "western"}, 8.9f,true, new String [] {"Clint Eastwood"}),
                            new Film("Madagaskar",new String [] {"comedy", "multfilm"}, 7.9f,false, new String [] {"Alex","Glorya","Melman","Marti"})
                    }
            );*/
            //myDBAdapter.showAllRecords(myDBAdapter.getAllRecords());

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    TableForm.createAndShowGUI(myDBAdapter);
                }
            });

            // ������� GUI


        } catch (PropertiesNotSetException e) {
            e.printStackTrace();
        }
    }
}
