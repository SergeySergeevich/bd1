package ru.com;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Umka on 20.09.2015.
 */
public class DBAdapter {
    private static String DB_URL = null;
    private static String USER = null;
    private static String PASSWORD = null;

    private static DBAdapter instance;

    private static final String [] table_names= new String[]{"actors","actors_in_films","films","films_genres","genres"};
    private static final String [] table_query_create= new String[]{
            "CREATE TABLE actors\n" +
                    "(\n" +
                    "  id_actor serial NOT NULL,\n" +
                    "  actor_name character varying(100),\n" +
                    "  CONSTRAINT actors_pkey PRIMARY KEY (id_actor),\n" +
                    "  CONSTRAINT actors_unique UNIQUE (actor_name)\n" +
                    ")",
            "CREATE TABLE actors_in_films\n" +
                    "(\n" +
                    "  id_film integer,\n" +
                    "  id_actor integer\n" +
                    ")",
            "CREATE TABLE films\n" +
                    "(\n" +
                    "  id_film serial NOT NULL,\n" +
                    "  film_name character varying(50),\n" +
                    "  rate real,\n" +
                    "  already_seen boolean,\n" +
                    "  CONSTRAINT films_pkey PRIMARY KEY (id_film),\n" +
                    "  CONSTRAINT film_unique UNIQUE (film_name)\n" +
                    ")",
            "CREATE TABLE films_genres\n" +
                    "(\n" +
                    "  id_film integer,\n" +
                    "  id_genre integer\n" +
                    ")",
            "CREATE TABLE genres\n" +
                    "(\n" +
                    "  id_genre serial NOT NULL,\n" +
                    "  genre_name character varying(50),\n" +
                    "  CONSTRAINT genres_pkey PRIMARY KEY (id_genre)\n" +
                    ")"};
    private DBAdapter() {
        try {
            // ��������� ��������
            System.out.println("DBAdapter(). . . " );
            Class.forName("org.postgresql.Driver").newInstance();
            // ��������� ������������� ������
            Connection connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
            System.out.println("Getting connection successfully. . . " );
            DatabaseMetaData meta = connection.getMetaData();

            for (int i = 0; i < table_names.length; i++) {
                ResultSet rs = meta.getTables(null, null, table_names[i], null);
                ResultSetMetaData rm = rs.getMetaData();
                if (rs.next()) {
                    System.out.println("Table '" + table_names[i]+"' exists");
                } else {
                    System.out.println("Table '" + table_names[i] + "' does not exist");
                    // �������
                    connection.createStatement().executeUpdate(table_query_create[i]);
                    System.out.println("Table '" + table_names[i] + "' created successfully ! ! !");
                }
            }
            connection.close();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void setProperties(String db_url, String db_user, String db_password){
        // ��������� �������� �����������
        DB_URL = db_url;
        USER = db_user;
        PASSWORD = db_password;
    }
    public static DBAdapter getInstance() throws PropertiesNotSetException {
        if (DB_URL==null || USER==null || PASSWORD==null) throw new PropertiesNotSetException();
        if (instance == null) {
            instance = new DBAdapter();
        }
        return instance;
    }
    public static void clearInstance() {
        if (instance != null) {
            instance=null;
        }
    }
    public void showRecordById(int id) throws SQLException {
        // ������ �������������� id ������, �������� �������� �������,
        // �� id �������� id ������
        // � �������
    }
    public void showRecordByFilmName(String film_name) throws SQLException {

    }
    public Film [] getAllRecords() throws SQLException {

        System.out.println("getAllRecords(). . . " );
        ArrayList <Film> films = new ArrayList<>(); // ������ ������

        Connection connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        System.out.println("Getting connection successfully. . . " );
        Statement statement = connection.createStatement();
        ResultSet resultSet;
        // ������ ������� ������


        /*
        ��������� ���������� �����������:
        �.�. ���� ��������� ���� �������, �������������� ����� ��������� ������� ���� ������� � ��� �����
        ����� ��������� ���������� �������� �� n-2

        ��� ������� ������ ������ ������� � ����������� ���� � ����� �� ������.
        �.�. ��������� ������� � ����� ��� ���������� ����������??

         */
        // �����
        HashMap<Integer,String> Genres = new HashMap<>(); // �������� �������� �� arraylist
        resultSet = statement.executeQuery("SELECT * FROM genres");
        while (resultSet.next()) {
            int id_genre = resultSet.getInt("id_genre");
            String genre_name = resultSet.getString("genre_name");
            Genres.put(id_genre,genre_name);
        }
        resultSet.close();
        // ������
        HashMap<Integer,String> Actors = new HashMap<>();
        resultSet = statement.executeQuery("SELECT * FROM actors");
        while (resultSet.next()) {
            int id_actor = resultSet.getInt("id_actor");
            String actor_name = resultSet.getString("actor_name");
            Actors.put(id_actor,actor_name);
        }
        resultSet.close();

        PreparedStatement getGenres = connection.prepareStatement("SELECT id_genre FROM films_genres WHERE id_film=?");
        PreparedStatement getActors = connection.prepareStatement("SELECT id_actor FROM actors_in_films WHERE id_film=?");

        String film_name;
        float rate;
        boolean already_seen;
        ArrayList<String> genres = new ArrayList<>();
        ArrayList<String> actors = new ArrayList<>();

        // ����� ������ ��-�� ������������� ������� ��������� resultSet �� ������ statement
        // ������� preparedstatements ��� actors � genres
        resultSet = statement.executeQuery("SELECT * FROM films");
        while (resultSet.next()) {
            film_name = resultSet.getString("film_name");
            rate = resultSet.getFloat("rate");
            already_seen = resultSet.getBoolean("already_seen");

            int film_id = resultSet.getInt("id_film");

            // �������� �����
            getGenres.setInt(1, film_id);
            ResultSet result_genres = getGenres.executeQuery();
            while (result_genres.next()){
                genres.add(Genres.get(new Integer(result_genres.getInt("id_genre"))));
            }
            result_genres.close();
            // �������� �������

            getActors.setInt(1,film_id);
            ResultSet result_actors = getActors.executeQuery();
            while (result_actors.next()){
                actors.add(Actors.get(new Integer(result_actors.getInt("id_actor"))));
            }
            result_actors.close();
            films.add(new Film(film_name, genres.toArray(new String[genres.size()]), rate, already_seen, actors.toArray(new String[actors.size()])));
            genres.clear();
            actors.clear();
        }
        resultSet.close();
        statement.close();
        connection.close();
        return films.toArray(new Film [films.size()]);
    }
    public void showAllRecords(Film [] films){
        for (Film fm: films)
            System.out.println(fm);
    }
    public int addRecords(Film [] films) throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        System.out.println("Getting connection successfully. . . " );
        Statement statement = connection.createStatement();
        int i;
        for (i = 0; i < films.length; i++) {
            PreparedStatement prepstat_1 = connection.prepareStatement("INSERT INTO films(film_name , rate , already_seen) VALUES (?,?,?)");
            //films
            prepstat_1.setString(1, films[i].getFilm_name());
            prepstat_1.setFloat(2, films[i].getRate());
            prepstat_1.setBoolean(3, films[i].isAlready_seen());
            prepstat_1.executeUpdate();
            // �������� id ������
            int id_film = 0;
            int id_actor = 0;
            int id_genre = 0;

            ResultSet resultSet;
            resultSet = statement.executeQuery("SELECT id_film FROM films WHERE film_name=" + "'" + films[i].getFilm_name() + "'");
            if (resultSet.next()) {
                id_film = resultSet.getInt("id_film");
            }//actors
            for (String act : films[i].getActors()) {  // ��� ������� ������
                resultSet = statement.executeQuery("SELECT id_actor FROM actors WHERE actor_name=" + "'" + act + "'");
                if(resultSet.next()) {
                    id_actor = resultSet.getInt("id_actor");
                    String query = "INSERT INTO actors_in_films(id_film, id_actor) VALUES(" +id_film+" , "+id_actor+")";
                    statement.executeUpdate(query);
                }
                else{ // ���� ���
                    String query = "INSERT INTO actors(actor_name) VALUES ('" + act +"')";// actors
                    statement.executeUpdate(query);
                    resultSet = statement.executeQuery("SELECT id_actor FROM actors WHERE actor_name="+"'"+act+"'" );
                    if (resultSet.next()) {
                        id_actor = Integer.parseInt(resultSet.getString(1));
                    }
                    query = "INSERT INTO actors_in_films(id_film, id_actor) VALUES(" +id_film+" , "+id_actor+")";
                    statement.executeUpdate(query);
                }
            }
            // genres
            for (String gen : films[i].getFilm_genres()) { // ��� ������� �����
                resultSet = statement.executeQuery("SELECT id_genre FROM genres WHERE genre_name=" + "'" + gen + "'");
                if(resultSet.next()) {
                    id_genre = Integer.parseInt(resultSet.getString(1));
                    String query = "INSERT INTO films_genres(id_film, id_genre) VALUES(" +id_film+" , "+id_genre+")";
                    statement.executeUpdate(query);
                }
                else{ // ���� ���
                    String query = "INSERT INTO genres(genre_name) VALUES ('" + gen +"')";// actors
                    statement.executeUpdate(query);
                    resultSet = statement.executeQuery("SELECT id_genre FROM genres WHERE genre_name=" + "'" + gen + "'");
                    if (resultSet.next()) {
                        id_genre = Integer.parseInt(resultSet.getString(1));
                    }
                    query = "INSERT INTO films_genres(id_film, id_genre) VALUES(" +id_film+" , "+id_genre+")";
                    statement.executeUpdate(query);
                }
            }
        }
        statement.close();
        connection.close();
        return i;
    }
    public int addRecord(Film film) throws SQLException {

        // �������� ����������
        Connection connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        System.out.println("Getting connection successfully. . . " );
        Statement statement = connection.createStatement();
        PreparedStatement prepstat_1 = connection.prepareStatement("INSERT INTO films(film_name , rate , already_seen) VALUES (?,?,?)");
        //films
        prepstat_1.setString(1, film.getFilm_name());
        prepstat_1.setFloat(2, film.getRate());
        prepstat_1.setBoolean(3, film.isAlready_seen());
        prepstat_1.executeUpdate();
        // �������� id ������
        int id_film = 0;
        int id_actor = 0;
        int id_genre = 0;

        ResultSet resultSet;
        resultSet = statement.executeQuery("SELECT id_film FROM films WHERE film_name=" + "'" + film.getFilm_name() + "'");
        if (resultSet.next()) {
            id_film = resultSet.getInt("id_film");
        }//actors
        for (String act : film.getActors()) {  // ��� ������� ������
            resultSet = statement.executeQuery("SELECT id_actor FROM actors WHERE actor_name=" + "'" + act + "'");
            if(resultSet.next()) {
                id_actor = resultSet.getInt("id_actor");
                String query = "INSERT INTO actors_in_films(id_film, id_actor) VALUES(" +id_film+" , "+id_actor+")";
                statement.executeUpdate(query);
            }
            else{ // ���� ���
                String query = "INSERT INTO actors(actor_name) VALUES ('" + act +"')";// actors
                statement.executeUpdate(query);
                resultSet = statement.executeQuery("SELECT id_actor FROM actors WHERE actor_name="+"'"+act+"'" );
                if (resultSet.next()) {
                    id_actor = Integer.parseInt(resultSet.getString(1));
                }
                query = "INSERT INTO actors_in_films(id_film, id_actor) VALUES(" +id_film+" , "+id_actor+")";
                statement.executeUpdate(query);
            }
        }
        // genres
        for (String gen : film.getFilm_genres()) { // ��� ������� �����
            resultSet = statement.executeQuery("SELECT id_genre FROM genres WHERE genre_name=" + "'" + gen + "'");
            if(resultSet.next()) {
                id_genre = Integer.parseInt(resultSet.getString(1));
                String query = "INSERT INTO films_genres(id_film, id_genre) VALUES(" +id_film+" , "+id_genre+")";
                statement.executeUpdate(query);
            }
            else{ // ���� ���
                String query = "INSERT INTO genres(genre_name) VALUES ('" + gen +"')";// actors
                statement.executeUpdate(query);
                resultSet = statement.executeQuery("SELECT id_genre FROM genres WHERE genre_name=" + "'" + gen + "'");
                if (resultSet.next()) {
                    id_genre = Integer.parseInt(resultSet.getString(1));
                }
                query = "INSERT INTO films_genres(id_film, id_genre) VALUES(" +id_film+" , "+id_genre+")";
                statement.executeUpdate(query);
            }
        }
        statement.close();
        connection.close();
        return id_film;
    }
    public void updateRecord() throws SQLException {

    }
    public void removeRecord() throws SQLException {

    }
    public void findByGenre(String film_genre) throws SQLException {

    }
    public void findByFilmName(String film_name) throws SQLException {

    }
    public void findByActor(String actor_name) throws SQLException {

    }
}
