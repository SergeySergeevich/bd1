package ru.com;

/**
 * Created by Umka on 20.09.2015.
 */
class Film {
    private String film_name;
    private String [] film_genres;
    private float rate;
    private boolean already_seen;
    private String [] actors;

    Film(String film_name,String [] film_genres, float rate, boolean already_seen, String [] actors){
        this.film_name=film_name;
        this.film_genres=film_genres;
        this.rate=rate;
        this.already_seen=already_seen;
        this.actors=actors;
    }

    @Override
    public String toString() {
        StringBuffer gBuffer = new StringBuffer();
        for (int i = 0; i < film_genres.length; i++) {
            gBuffer.append(film_genres[i]);
            if (i!=film_genres.length) gBuffer.append(", ");
            else gBuffer.append('.');
        }
        StringBuffer aBuffer = new StringBuffer();
        for (int i = 0; i < actors.length; i++) {
            aBuffer.append(actors[i]);
            if (i!=actors.length) aBuffer.append(", ");
            else aBuffer.append('.');
        }
        return "film: " + film_name + " \n" +
                "genres: " + gBuffer.toString() + " \n" +
                "rate: " + rate + " \n" +
                "Have you seen this movie before? : " + already_seen + " \n" +
                "In action: " + aBuffer.toString() + " \n";
    }


    public String getFilm_name() {
        return film_name;
    }

    public void setFilm_name(String film_name) {
        this.film_name = film_name;
    }

    public String[] getFilm_genres() {
        return film_genres;
    }

    public void setFilm_genres(String[] film_genres) {
        this.film_genres = film_genres;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public boolean isAlready_seen() {
        return already_seen;
    }

    public void setAlready_seen(boolean already_seen) {
        this.already_seen = already_seen;
    }

    public String[] getActors() {
        return actors;
    }

    public String getActorsInString() {
        StringBuffer act = new StringBuffer();
        for (int i=0;i<actors.length;i++) {
            act.append(actors[i]);
            if (i!=actors.length-1) act.append(", ");
        }
        return act.toString();
    }
    public String getGenresInString() {
        StringBuffer gen = new StringBuffer();
        for (int i=0;i<film_genres.length;i++) {
            gen.append(film_genres[i]);
            if (i!=film_genres.length-1) gen.append(", ");
        }
        return gen.toString();
    }
    public void setActors(String[] actors) {
        this.actors = actors;
    }

    public static void main(String[] args){
        System.out.println(new Film("Madagaskar: New War",new String [] {"comedy","military", "multfilm"}, 4.5f ,false, new String [] {"Alex","Glorya","Melman","Marti"}));
        System.out.println(new Film("Madagaskar: New War",new String [] {"comedy","military", "multfilm"}, 4.5f ,false, new String [] {"Alex","Glorya","Melman","Marti"}));
    }
}
