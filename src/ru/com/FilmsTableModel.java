package ru.com;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Umka on 24.09.2015.
 */
public class FilmsTableModel implements TableModel {
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
    private List<Film> films;
    String[] columnNames = {"film",
            "genres",
            "actors",
            "rate",
            "already seen?"};

    public FilmsTableModel(List<Film> films) {
        this.films = films;
    }

    @Override
    public int getRowCount() {
        return films.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int i) {
        switch (i){
            case 0:
                return columnNames[i];
            case 1:
                return columnNames[i];
            case 2:
                return columnNames[i];
            case 3:
                return columnNames[i];
            case 4:
                return columnNames[i];
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int i) {
        switch (i){
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return Float.class;
            case 4:
                return Boolean.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Film film = films.get(rowIndex);
        switch (columnIndex){
            case 0:
                return film.getFilm_name();
            case 1: // genres
                return film.getGenresInString();
            case 2: // actors
                return film.getActorsInString();
            case 3:// rate
                return film.getRate();
            case 4://
                return film.isAlready_seen();
            default:
                return "error";
        }
    }

    @Override
    public void setValueAt(Object o, int i, int i1) {

    }

    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {
        listeners.add(tableModelListener);
    }

    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {
        listeners.remove(tableModelListener);
    }
}
